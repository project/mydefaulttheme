<?php

function mydefaulttheme_preprocess_html(&$variables) {
  $this_settings = theme_get_setting('palette');
  if(isset($this_settings['titlecolor'], $this_settings['link'],  $this_settings['text']) && file_exists(variable_get('file_public_path', conf_path() . '/files') ."/mydefaulttheme_header_bg.png")){
  
  $custom_css = " body {\n   color: ".$this_settings['text'].";\n   }\n   #header {\n   background-image: url(".base_path().variable_get('file_public_path', conf_path() . '/files') ."/mydefaulttheme_header_bg.png);\n   }\n   a:link, a:visited, #mission a, #mission a:visited, a:hover {\n   color: ".$this_settings['link'].";\n   }\n   ul.links li {\n   border-left: 1px solid ".$this_settings['link'].";\n   }\n   #subnavlist, #subnavlist a, #logo a.site-name, .site-slogan {\n   color: ".$this_settings['titlecolor'].";\n  }\n ";
   drupal_add_css($custom_css, array('type' => 'inline'));
} else { 
  $custom_css = drupal_get_path('theme', 'mydefaulttheme')."/images/default.css";
  drupal_add_css($custom_css, array('type' => 'file'));
 }
}
?>
