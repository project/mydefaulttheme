<?php
/**
* @file page.tpl.php
* Default theme implementation to display a single Drupal page.

Available variables:

General utility variables:

    * $base_path: The base URL path of the Drupal installation. At the very least, this will always default to /.
    * $directory: The directory the template is located in, e.g. modules/system or themes/bartik.
    * $is_front: TRUE if the current page is the front page.
    * $logged_in: TRUE if the user is registered and signed in.
    * $is_admin: TRUE if the user has permission to access administration pages.

Site identity:

    * $front_page: The URL of the front page. Use this instead of $base_path, when linking to the front page. This includes the language domain or prefix.
    * $logo: The path to the logo image, as defined in theme configuration.
    * $site_name: The name of the site, empty when display has been disabled in theme settings.
    * $site_slogan: The slogan of the site, empty when display has been disabled in theme settings.

Navigation:

    * $main_menu (array): An array containing the Main menu links for the site, if they have been configured.
    * $secondary_menu (array): An array containing the Secondary menu links for the site, if they have been configured.
    * $breadcrumb: The breadcrumb trail for the current page.

Page content (in order of occurrence in the default page.tpl.php):

    * $title_prefix (array): An array containing additional output populated by modules, intended to be displayed in front of the main title tag that appears in the template.
    * $title: The page title, for use in the actual HTML content.
    * $title_suffix (array): An array containing additional output populated by modules, intended to be displayed after the main title tag that appears in the template.
    * $messages: HTML for status and error messages. Should be displayed prominently.
    * $tabs (array): Tabs linking to any sub-pages beneath the current page (e.g., the view and edit tabs when displaying a node).
    * $action_links (array): Actions local to the page, such as 'Add menu' on the menu administration interface.
    * $feed_icons: A string of all feed icons for the current page.
    * $node: The node object, if there is an automatically-loaded node associated with the page, and the node ID is the second argument in the page's path (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).

Regions:

    * $page['help']: Dynamic help text, mostly for admin pages.
    * $page['highlighted']: Items for the highlighted content region.
    * $page['content']: The main content of the current page.
    * $page['sidebar_first']: Items for the first sidebar.
    * $page['sidebar_second']: Items for the second sidebar.
    * $page['header']: Items for the header region.
    * $page['footer']: Items for the footer region.

*/

 //print render($page['header']);?>

<table cellpadding="0" cellspacing="0" border="0" id="header">
 <tr>
  <td id="hl">
  </td>
  <td id="hm">
   <div id="logo">
    <?php if ($logo) { ?><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    <?php  if ($site_name) { ?><h1><a  class='site-name' href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
       <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
   </div>
  </td>
  <td id="h-subnav">
   <?php if (isset($secondary_menu)) { print theme('links__system_secondary_menu', array(
      'links' => $secondary_menu,
      'attributes' => array(
        'id' => 'subnavlist',
        'class' => array('links'),
      ),
    )); } ?>
  </td>  
  <td id="hr">
  </td>
 </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" id="header-links">
 <tr>
  <td class="cl">
  </td>
  <td class="header-links">
    <?php if (isset($main_menu)) {  print theme('links__system_main_menu', array(
      'links' => $main_menu,
      'attributes' => array(
        'id' => 'navlist',
        'class' => array('links'),
      ),
    )); } ?>
  </td>
  <td class="cr">
  </td>
 </tr>
</table> 
<table cellpadding="0" cellspacing="0" border="0" id="content">
 <tr>
   <?php if ($page['sidebar_first']){ ?><td id="sidebar-first">
   <?php print render($page['sidebar_first']); ?>
      </td><?php } else { ?><td class="cl"></td><?php } ?>
  <td valign="top" id="main">
        <?php if ($page['highlighted']) { ?>
       <div id="mission"><?php print render($page['highlighted']); ?>
       </div><?php } ?>
        <div id="main-content">
        <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
         </div>
        </td>
       <?php if ($page['sidebar_second']) { ?><td id="sidebar-second">
       <?php print render($page['sidebar_second']); ?>
       </td><?php } else { ?><td class="cr"></td><?php } ?>
      </tr>
         </table>
    <table cellpadding="0" cellspacing="0" border="0" id="footer">
           <tr>
       <td id="f-left">
       </td>
       <td id="f-middle">
        <div class="inner-content">
        <?php print render($page['footer']);?>
        </div>
       </td>
       <td id="f-right">
       </td>
      </tr>
         </table>
<!--Sponsored links (!don't delete this links)-->
<p><a href="http://sw.bi3.biz" style="font-size: 0.8em;" title="Kostenlose Software">bi3</a> | <a href="http://drupal.org/project/mydefaulttheme" rel="nofollow" style="font-size: 0.8em;">Download this Theme</a></p>
