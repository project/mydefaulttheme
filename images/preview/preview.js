  (function ($) {
   Drupal.color = {
    callback: function() {
     change_palette();
     }
    }
  $('#edit-scheme').change(function() {
  change_palette();
  });
  function change_palette(){
  $('.preview-site-slogan').css('color',$('#edit-palette-titlecolor').val());
  $('#preview-body a').css('color',$('#edit-palette-link').val());
  $('#preview-title a').css('color',$('#edit-palette-titlecolor').val());
  $('.preview-slogan').css('color',$('#edit-palette-titlecolor').val());
  $('#preview-subnav a').css('color',$('#edit-palette-titlecolor').val());
  $('#preview-content').css('color',$('#edit-palette-text').val());
  $('#preview-footer').css('color',$('#edit-palette-text').val());
  $('#preview-sidebar').css('color',$('#edit-palette-text').val());
  var upper_color = $('#edit-palette-top').val();
  var lower_color = $('#edit-palette-bottom').val();
  $('#preview-body').css('background-image','url('+Drupal.settings.basePath+'themes/mydefaulttheme/images/header-bg.php?palette-top='+upper_color.substr(1)+'&palette-bottom='+lower_color.substr(1)+')'); 

 }
}(jQuery));
