<?php
// If we don't have image processing support, redirect.
if ( !function_exists('imagecreate') )
	die(header("Location: header-bg.jpeg"));
include('grandient.class.php');
header("Content-Type: image/jpeg");
$grand = new phpgrandient;
$grand->draw();
?>
