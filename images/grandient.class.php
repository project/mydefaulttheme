<?php
/**
 * @Source code from Wordpress subpackage Default_Theme
 * @url: http://wordpress.org/extend/themes/default
 */
class phpgrandient {
var $default = false;
var $h = 200;
var $w = 1;
var $output = '';
function draw(){
$vars = array('palette-top'=>array('r1', 'g1', 'b1'), 'palette-bottom'=>array('r2', 'g2', 'b2'));
foreach ( $vars as $var => $subvars ) {
	if ( isset($_GET[$var]) ) {
		foreach ( $subvars as $index => $subvar ) {
			$length = strlen($_GET[$var]) / 3;
			$v = substr($_GET[$var], $index * $length, $length);
			if ( $length == 1 ) $v = '' . $v . $v;
			$$subvar = hexdec( $v );
			if ( $$subvar < 0 || $$subvar > 255 )
				$this->default = true;
		}
	} else {
		$this->default = true;
	}
}
if ($this->default)
	list ( $r1, $g1, $b1, $r2, $g2, $b2 ) = array ( 105, 174, 231, 65, 128, 182 );
// Create the image
$im = imagecreate($this->w, $this->h);
imagecolorallocate($im, 231, 231, 231);
// Draw a new color thing
for ( $i = 0; $i < $this->h; $i++ ) {
	$x1 = 0;
	$x2 = 1;
	$r = ( $r2 - $r1 != 0 ) ? $r1 + ( $r2 - $r1 ) * ( $i / $this->h ) : $r1;
	$g = ( $g2 - $g1 != 0 ) ? $g1 + ( $g2 - $g1 ) * ( $i / $this->h ) : $g1;
	$b = ( $b2 - $b1 != 0 ) ? $b1 + ( $b2 - $b1 ) * ( $i / $this->h ) : $b1;
	$color = imagecolorallocate( $im, $r, $g, $b );
	imageline( $im, $x1, $i + 18, $x2, $i + 18, $color );
}
imagejpeg($im, $this->output, 92);
imagedestroy($im);
}
 }
?>
