<?php

function mydefaulttheme_form_system_theme_settings_alter(&$form, &$form_state) {
  if (module_exists('color')){
  $theme = 'mydefaulttheme';
  $theme_path = drupal_get_path('theme', $theme);
  $base = drupal_get_path('module', 'color');
  $logo_path = base_path().$theme_path.'/images/preview/logo.png';
  include_once('images/preview/color.inc');
  include_once('images/preview/preview.inc');
  drupal_add_css($theme_path.'/images/preview/preview.css', array('type' => 'file'));
  drupal_add_js($theme_path.'/images/preview/preview.js', array('type' => 'file'));
  $color_sets = array();
  $schemes = array();
  $current_palette = theme_get_setting('palette');
  $current_scheme = theme_get_setting('scheme');
  foreach ($info['schemes'] as $key => $scheme) {
    $color_sets[$key] = $scheme['title'];
    $schemes[$key] = $scheme['colors'];
  }

  // See if we're using a predefined scheme.
  // Note: we use the original theme when the default scheme is chosen.

  foreach ($schemes as $key => $scheme) {
    if ($current_scheme == $key) {
      $scheme_name = $key;
      break;
    }
  }
  if (empty($scheme_name)) {
    if (empty($current_scheme)) {
      $scheme_name = 'default';
    }
    else {
      $scheme_name = '';
    }
  }

  // Add scheme selector.
  $form['scheme'] = array(
    '#type' => 'select',
    '#title' => t('Color set'),
    '#options' => $color_sets,
    '#default_value' => $scheme_name,
    '#prefix' => '<fieldset id="color_scheme_form" class="form-wrapper"><legend><span class="fieldset-legend">'.t('Color scheme').'</span></legend><div class="fieldset-wrapper"><div class="color-form clearfix">', 
    '#suffix' => '<div id="palette" class="clearfix">',
    '#attached' => array(
      // Add Farbtastic color picker.
      'library' => array(
        array('system', 'farbtastic'),
      ),
      // Add custom CSS.
      'css' => array(
        $base . '/color.css' => array(),
      ),
      // Add custom JavaScript.
      'js' => array(
        $base . '/color.js',
        array(
          'data' => array(
            'color' => array(
              'reference' => color_get_palette($theme, TRUE),
              'schemes' => $schemes,
            ),
          ),
          'type' => 'setting',
        ),
      ),
    ),
  );

  // Add palette fields.
  $palette = $info['schemes']['default']['colors'];
  $names = $info['fields'];
  $form['palette']['#tree'] = TRUE;
  foreach ($palette as $name => $value) {
    if (isset($names[$name])) {
      $form['palette'][$name] = array(
        '#type' => 'textfield',
        '#title' => check_plain($names[$name]),
        '#default_value' => $value,
        '#size' => 8,
      );
    }
  }
  

  $form['info'] = array('#type' => 'value', '#value' => $info, '#suffix' => $preview_html.'</div></div></div></fieldset>');
  $form['#submit'][] = 'mydefaulttheme_form_system_theme_settings_submit';
  return $form;
 }
}


function  mydefaulttheme_form_system_theme_settings_submit (&$form, &$form_state) {
  if(isset($_POST["palette"]["top"], $_POST["palette"]["bottom"]) && function_exists('imagecreate')){
   $_GET["palette-top"] = str_replace('#','',$_POST["palette"]["top"]);
   $_GET["palette-bottom"] = str_replace('#','',$_POST["palette"]["bottom"]);
   include_once(drupal_get_path('theme', 'mydefaulttheme').'/images/grandient.class.php');
   $grand = new phpgrandient;
   $grand->output = variable_get('file_public_path', conf_path() . '/files') ."/mydefaulttheme_header_bg.png";
   $grand->draw();
   
 }
  }
?>
